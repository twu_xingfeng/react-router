import React, {Component} from 'react';
import '../styles/App.css';
import {BrowserRouter, Link} from 'react-router-dom';
import Home from "./Home";
import Profile from "./Profile";
import About from "./About";
import {Route} from "react-router";
import Products from "./Products";
import ProductDetail from "./ProductDetail";
import jsonData from '../data.json';

class App extends Component {

    constructor(props){
        super(props);
    }

    render() {
    return (
      <div className="app">
        <BrowserRouter>
            <ul>
                <li><Link to='/'>HOME</Link></li>
                <li><Link to='/profile'>PROFILE</Link></li>
                <li><Link to={{
                    pathname: "/products",
                    state: { data: jsonData}
                }}>PRODUCTS</Link></li>
                <li><Link to='/about'>ABOUT</Link></li>
            </ul>
            <section className="container">
          <Route exact path='/' component={Home}/>
          <Route path='/profile' component={Profile}/>
          <Route path='/products' component={Products}/>
          <Route path='/details/:id' component={ProductDetail}/>
          <Route path='/about' component={About}/>
                <img className={"img"} src={"https://www.thoughtworks.com/imgs/tw-logo.png"}/>
            </section>
        </BrowserRouter>
      </div>
    );
  }
}

export default App;
