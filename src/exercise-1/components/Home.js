import {Fragment, default as React} from "react";


export default class Home extends React.Component{

    constructor(props){
        super(props);
    }


    render(){
        return (
            <Fragment>
                <h2>HOME</h2>
                <p>This is  a <strong>beautiful</strong> Home Page</p>
            <p>and the url is '/'</p>
            </Fragment>
        )
    }
};