import {Fragment, default as React} from "react";
import {Link} from "react-router-dom";


export default class ProductDetail extends React.Component{

    constructor(props){
        super(props);
    }


    render(){
        return (
            <Fragment>
                <h2>DETAIL</h2>
                <p>{this.props.location.state.data.id}</p>
                <p>{this.props.location.state.data.name}</p>
                <p>{this.props.location.state.data.desc}</p>

            </Fragment>
        )
    }
};