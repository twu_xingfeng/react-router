import {Fragment, default as React} from "react";
import {Link} from "react-router-dom";

export default class Products extends React.Component {

    constructor(props) {
        super(props);
    }


    render() {

        let ul = [];
        console.log(this.props.location.state.data);
        for (let key  in this.props.location.state.data) {
            ul.push(<section>
                <Link to={{
                    pathname: "/details/" + this.props.location.state.data[key].id,
                    state: {data: this.props.location.state.data[key]}
                }}>
                    {key}
                </Link>
            </section>);
        }
        ;
        return (
            <Fragment>
                <h2>PRODUCTS</h2>
                <section>
                    {ul}
                </section>

            </Fragment>
        )
    }
};